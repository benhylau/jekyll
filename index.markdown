---
layout: start
title: Open source cooperative video conferencing platform, meet.coop
---

**The Online Meeting Co-operative provides an open source, meeting and conferencing platform, powered by [BigBlueButton](https://bigbluebutton.org/).**

We have a [demonstration meeting server at `demo.meet.coop`](https://demo.meet.coop/) with open account creation. Please try the service out. At this current stage, we do not guarantee it will always be available and end-users should not rely on your rooms and recordings being saved and always available. This server doesn't have the resources that are dedicated to our production servers.

Our [production meeting server at `ca.meet.coop`](https://ca.meet.coop) is open to members of our co-operative and is available for large online events and conferences. Please contact us by email at [contact@meet.coop](mailto:contact@meet.coop) or via our [open forum](https://forum.meet.coop/) if you would like to discuss using our services for your organisation either for on-going meeting or big events or conferences, our `ca.meet.coop` server has a lot of resources and is provided by [Koumbit](https://www.koumbit.org/en) and is running on green energy in Canada. 
