---
title: Get involved with meet.coop!
---

The [Online Meeting Cooperative](/) project is raising a lot of interest and ideas and people are asking how they can participate. We aim to self organise as a community to enable everyone to contribute and help out others. Following are a few points of entry.

## Wiki

We are documenting the project at [wiki.meet.coop](https://wiki.meet.coop/).

## Forum

We have an open discussion forum at [forum.meet.coop](https://forum.meet.coop/).

## Social Networks

We encourage everyone to engage about The Online Meeting Cooperative in social networks. There’s no «official» account and we suggest to use the same hashtags to facilitate connections. **#meetcoop**

## Become a Member

The Online Meeting Cooperative is organised through a group of cooperatives, some of which take responsibility for the memberships and finance. We invite everyone to become a member of one of these cooperatives and make your contribution through them. Read more about at [Memberships](https://wiki.meet.coop/wiki/Membership).

## Code

We host all our code at the cooperative GitLab instance at [git.coop/meet](https://git.coop/meet).

## Contact

If you want to contribute otherwise, please contact us at [contact@meet.coop](mailto:contact@meet.coop).
